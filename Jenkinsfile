#!/usr/bin/env groovy

// stores reference to the locally built docker image
def buildImage
// tag that will be used for the docker image when pushed to registry
def buildTag

pipeline {
    environment {
        deployUser = "deploy"
        deployPath = "/var/www/ingest_api/fits"
        deployServer = "ingest01.sls.fi"
        registry = "slsfinland/metadata-extractor"
        // Id of credentials stored in SLS Jenkins to auth against slsfinland docker hub registry
        registryCredential = 'dockerhub_credentials'

        artifactVersion = "1.5.1"
    }
    agent {
        label 'master'
    }

    stages {

        stage('Build Docker image') {
            steps {
                script {
                    if(env.BRANCH_NAME == 'master'){
                        buildTag = artifactVersion
                    } else if(env.BRANCH_NAME.startsWith("release/")){
                        buildTag = 'rc'
                    } else {
                        buildTag = 'dev'
                    }

                    buildImage = docker.build("${registry}:${buildTag}", "./docker")
                }
            }
        }

        stage('Publish docker image') {
            steps {
                script {
                    docker.withRegistry( '', registryCredential ) {
                        buildImage.push()
                    }
                }
            }
        }

        stage('Release to production') {
            when {
                branch "master"
                beforeInput true
            }
            input { message 'Will release to PRODUCTION, do you want to proceed?'}

            steps {
                sshagent (credentials: ["${deployUser}"]) {
                    sh "ssh -l ${deployUser} ${deployServer} 'mkdir -p ${deployPath}'"
                    sh '''
                        scp ${WORKSPACE}/docker-compose.yml \
                         ${WORKSPACE}/docker-compose.prod.yml \
                         ${deployUser}@${deployServer}:${deployPath}/
                    '''
                    sh "ssh -l ${deployUser} ${deployServer} 'cd ${deployPath} && docker-compose -f docker-compose.yml -f docker-compose.prod.yml config > ingest_fits-swarm.yml'"
                    sh "ssh -l ${deployUser} ${deployServer} 'cd ${deployPath} && docker stack deploy --compose-file ingest_fits-swarm.yml sls_ingest_fits'"
                }
            }
        }
    }
}