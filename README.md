# SLS FITS image

Based on the official https://hub.docker.com/r/harvardlts/fitsservlet_container, adding configuration changes to support the SLS usecases

Note: version numbers for the docker image tags and sls-* git tags follow the same major.minor version as the upstream project, but not necessarily the same patch versions (because we might do modifications to our configs, which are reflected in the patches)

## Note on tomcat user
The Dockerfile adds the tomcat user to the linux group with id 10059 and changes the user id to 11088 when the docker image is built. This is because on the test server the Projekt-mount where ingestable files are located is accessible by that group and user. The metadata-extractor image needs read privileges to the projects folder structure, which the tomcat user inside the docker container does not have unless added to that gid. Hard coding the user group is not ideal, but atm docker-compose / swarm does not support defining group membership from outside a container.

## Build new Docker image and push to hub



### Automated builds

The SLS Jenkins instance is set up with a Jenkins pipeline to build and push new versions of this image to Docker hub. See Jenkinsfile in this project for steps.
The build needs to be triggered manually in Jenkins for the time being.

When a change is pushed to the develop branch, the pipeline builds an image with the :dev tag. When a change is pushed to the master branch, the pipeline builds a image with a versioned tag. The tag version nr is taken from the artifactVersion variable found in the Jenkinsfile, and must be updated there.

To move a new release to production one must also update the docker-compose files with the new version number. This should be done in a release branch preparation that is then merged to master and pushed to git, after which the master branch can be built in Jenkins.

### To build the image manually
```
docker build docker/. -t slsfinland/metadata-extractor:1.5.1
```

Once the image build is complete, you can test it on your local machine by running
```
docker container run -d --name fits --publish 8080:8080 slsfinland/metadata-extractor:1.5.1
```
And then navigating to http://localhost:8080 to test.

If the interface works, the new image can be pushed to docker hub using
```
docker push slsfinland/metadata-extractor:1.5.1
```


### Override config values of the docker container FITS instance

The xml files that configure the fits service are stored in the path ```/opt/fits/xml``` inside the container. We copy config files in the ```<project_root>/docker/xml``` folder into the image at build time. If you want to override configuration without creating a new image, you can do so by bind mounting the correspoinding config file to the path inside the container. E.g., to override fits.xml:

```
docker run -v <path-to>/fits.xml:/opt/fits/xml/fits.xml slsfinland/metadata-extractor:latest
```
